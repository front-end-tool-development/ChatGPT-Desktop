## mac 系统文件两种模式：

因此，.app 文件是一个独立的应用程序包，用户可以直接运行，而.dmg 文件则是用于分发应用程序或文件的压缩磁盘映像格式。

## 前置条件：Tauri 开发 build

https://tauri.app/zh-cn/v1/guides/getting-started/prerequisites

（1）您将需要安装 CLang 和 macOS 开发依赖项。 为此，您需要在终端中执行以下命令：
xcode-select --install

（2）安装好 Rust
rustup

## 打包记录

这里 package.json 内容 build 后没有可执行文件，所以这里看 tauri 后，查看；

## Tauri 使用打包

第一步：
npm install -g @tauri-apps/cli
注意：查看，是不是安装好：tauri 的相关依赖正确，如果不正确，修改 yi
第二步：
tauri init

第三步：
tauri build

## Windows 安装程序

pnpm tauri build
https://tauri.app/zh-cn/v1/guides/building/windows

## macOS Bundle

mac 打包报错：  
Error A public key has been found, but no private key. Make sure to set `TAURI_PRIVATE_KEY` environment variable.

### 解决

上面问题说：当前的公钥找到了，但是没有找到私钥，那么需要创建一个；
（1）第一步：生成 pem 的私钥
openssl genrsa -out private_key.pem 2048
执行后：在当前的文件中生成一个：private_key.pem 文件；
（）第二步：需要将当前的全局的变量设置一下：
export TAURI_PRIVATE_KEY=/Users/henryning/Documents/code/personCode/Tauri-ChatGPT-Desktop/private_key.pem
